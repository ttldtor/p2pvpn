# P2P VPN

A P2P VPN fork, originally created by Wolfgang Ginolas (http://www.p2pvpn.org/).

![Screenshot](repo/screenshot.png)

# How to use

1. On Windows, install the TAP-Win32 Adapter OAS driver by running `add_driver.bat` in the driver directory (preferably as administrator).
2. Run `P2PVPN.exe`.

If running on Linux, the application needs to be run as root via `sudo java -jar P2PVPN.jar` or `p2pvpn-pkexec`.

# Running in the background

```
sudo java -jar P2PVPN.jar --headless --access=access.dat --name=Supernode --port=2103 --vpn.ip=10.6.10.10 --vpn.mask=255.255.0.0
```

where `access.dat` is a _private_ invitation.

Run P2PVPN with the `--help` parameter for extra documentation.

# Creating a network

To create a new network, you need to specify its name. Other values are: 
- VPN network address (ex. `10.6.0.0`);
- Subnet mask (ex. `255.255.0.0`);
- BitTorrent tracker HTTP address;
- DHT search;
- Known hosts to which participants will join by default.

The default number of participants is 2¹⁶, which corresponds to the subnet mask.

Next, you need to send invitations to users. There are two types of "certificates" in the application: a _network_ certificate (analogous to the root certificate) and an _access_ certificate, which is signed by the network certificate.

Clicking "Allow invitation of others" will generate an invitation based on the network private keys, which means that the invited users can invite as many other users as they like and create as many private invitations as they like.

If you disable this option, only a private invitation will be generated that can be given an expiration time.

**Do not edit the invitation file** otherwise the signature verification will fail. The `network.*` parameters are checked on the client-side when the invitation text/file is imported, and the `access.*` parameters are checked on the other user side when peers are connecting to each other.

# Joining a network

To join the network, all you have to do is simply accept the invitation. To do this, you need to click on the accept invitation button and import the invitation file into the application or copy its contents into the input field. If the OK button does not appear enabled, the invitation has not passed the verification check.

If the connection to the peers didn't happen through BitTorrent tracker, you can connect to any of the peers manually by specifying `ip:port` in the information window and retrieve other network members through this peer.

A direct connection between all members of the network may not occur immediately. It's sufficient for at least one network member to have a port forwarded. If both network members are behind NAT, they can only connect through a third member.

# Security

The following cryptographic algorithms are used in the application:

- `RSA-2048` for peer authefication.
- `AES-128-CBC` for encrypting peer-to-peer trafic.
- `SHA-512 with RSA` algorithm is used to sign invitations.
