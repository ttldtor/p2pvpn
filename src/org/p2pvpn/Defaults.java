/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023 Nikolay Borodin <Monsterovich>

    This file is part of P2PVPN.

    P2PVPN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    P2PVPN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with P2PVPN.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn;

/**
 * Default values
 * @author Monsterovich
 */
public class Defaults {
    public static class NewNetwork {
        final public static String NAME = "New network";
        final public static String NETWORK = "10.6.0.0";
        final public static String SUBNET = "255.255.0.0";
        final public static String BITTORRENT_TRACKER = "http://p2p.0g.cx:6969/announce";
    }
}
