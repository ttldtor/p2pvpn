/*
    Copyright 2023 Nikolay Borodin <Monsterovich>

    This file is part of P2PVPN.

    P2PVPN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    P2PVPN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with P2PVPN.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.gui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * The custom implementation of TrayIcon
 * @author Monsterovich
 */
public final class AppIndicatorTrayIcon extends TrayIcon {
    
    static String TRAYICON_TMP_FILE = "P2PVPN_Tray.png";
    private String icon;
    
    static {
        String libname = null;
        try {
            switch (System.getProperty("os.arch")) {
                case "x86":
                case "i386":
                case "i486":
                case "i586":
                case "i686":
                    libname = "clib/libAppIndicator.so";
                    break;
                case "x86_64":
                case "amd64":
                    libname = "clib/libAppIndicator64.so";
                    break;
                default:
                    Logger.getLogger("").log(Level.WARNING, "Unknown os.arch. Still trying to load libAppIndicatorUnknown.so");
                    libname = "clib/libAppIndicatorUnknown.so";
                    break;
            }
            File file = new File(libname);
            if (file.exists()) {
                System.load(file.getCanonicalPath());
            } else {
                throw new Exception(String.format("loadLib failed. Library file %s is not found!", libname));
            }
            System.load(file.getCanonicalPath());
        } catch (Throwable e) {
            Logger.getLogger("").log(Level.SEVERE, "Could not load App indicator library", e);
        }
    }
    
    public AppIndicatorTrayIcon(final Image image, final String tooltip, final PopupMenu popup) {
        super(image, tooltip, popup);

        // library status check
        try {
            test();
        } catch (UnsatisfiedLinkError e) {
            return;
        }

        new Thread(new Runnable() {
            public void run() {
                // all natives must run in the same thread.
                for (int i = 0; i < popup.getItemCount(); i++) {
                    addMenu(popup.getItem(i).getLabel());
                }

                // extract the icon from the resources, as we have to load it from the file into the tray icon library
                try {
                    File output = new File(TRAYICON_TMP_FILE);
                    BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 
                        BufferedImage.TYPE_INT_ARGB);

                    Graphics2D graphics = bufferedImage.createGraphics();
                    graphics.drawImage(image, 0, 0, null);
                    graphics.dispose();

                    ImageIO.write(bufferedImage, "png", output);
                    icon = output.getCanonicalPath();

                    trayRun(icon, tooltip);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    
    public void clickCallback(int id) {
        for (ActionListener listener : this.getPopupMenu().getItem(id).getActionListeners()) {
            listener.actionPerformed(null);
        }
    }
    
    @Override
    public void displayMessage(String caption, String text, MessageType messageType) {
        try {
            showMessage(caption, text, icon);
        } catch (UnsatisfiedLinkError e) {
            super.displayMessage(caption, text, messageType);
        }
    }

    public native void test();

    private native void trayRun(String icon, String tooltip);
    private native void addMenu(String text);
    private native void showMessage(String caption, String text, String icon);
}
