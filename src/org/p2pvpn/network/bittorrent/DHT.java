/*
    Copyright 2008, 2009 Wolfgang Ginolas

    This file is part of P2PVPN.

    P2PVPN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    P2PVPN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with P2PVPN.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network.bittorrent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.p2pvpn.network.ConnectionManager;
import org.p2pvpn.network.bittorrent.bencode.Bencode;
import org.p2pvpn.network.bittorrent.bencode.BencodeInt;
import org.p2pvpn.network.bittorrent.bencode.BencodeList;
import org.p2pvpn.network.bittorrent.bencode.BencodeMap;
import org.p2pvpn.network.bittorrent.bencode.BencodeObject;
import org.p2pvpn.network.bittorrent.bencode.BencodeString;
import org.p2pvpn.tools.CryptoUtils;
import org.p2pvpn.network.Connector.Endpoint;

// https://www.bittorrent.org/beps/bep_0005.html
public class DHT implements Runnable {

    private static final String[] DHT_BOOTSTRAP_NODES = {
        "router.bittorrent.com:6881",
        "dht.transmissionbt.com:6881",
        "dht.aelitis.com:6881",
        "dht.libtorrent.org:25401"
    };
    
    private ConnectionManager connectionManager;
    final private Thread receivingThread;
    private boolean receivingThreadRunning = true;

    private static final int PACKET_LEN = 10 * 1024;
    private static final BigInteger MASK = new BigInteger("1").shiftLeft(160);
    final private static int MAX_BAD = 4;
    final private static int MAX_GOOD = 4;
    final private static int NODE_ID_LEN = 20;
    final private static int NODE_IPV4_LEN = 6; // 4 bytes for ipv4 + 2 bytes for port 
    final private static int NODE_IPV6_LEN = 18; // 16 bytes for ipv6 + 2 bytes for port
    final private static int PEER_ANNOUNCE_INTERVAL_MS = 15 * 60 * 1000;


    private DatagramSocket dSock;

    private BencodeString id;
    private BencodeString searchID;
    private BigInteger searchIDInt;
    
    final private PriorityBlockingQueue<Contact> peerQueue;

    final private Map<Contact, Integer> peerBad;
    final private Map<Contact, Long> announcePeerLastTime;

    public DHT(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        
        byte[] peerId = new byte[NODE_ID_LEN];
        new Random().nextBytes(peerId);
        id = new BencodeString(peerId);

        searchID = new BencodeString(networkHash(NODE_ID_LEN));
        searchIDInt = unsigned(new BigInteger(searchID.getBytes()));

        try {
            dSock = new DatagramSocket(null);
            dSock.setReuseAddress(true);
            dSock.bind(new InetSocketAddress(connectionManager.getServerPort()));
        } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, "Couldn't bind DHT on port " + connectionManager.getServerPort(), e);
        }

        peerQueue = new PriorityBlockingQueue<Contact>(10, new Comparator<Contact>() {
            public int compare(Contact o1, Contact o2) {
                BigInteger d1 = searchDist(o1);
                BigInteger d2 = searchDist(o2);
                return d1.compareTo(d2);
            }
        });

        peerBad = Collections.synchronizedMap(new HashMap<Contact, Integer>());

        announcePeerLastTime = new HashMap<Contact, Long>();

        receivingThread = new Thread(new Runnable() {
            public void run() {
                runReceivingThread();
            }
        });
        receivingThread.start();

        this.bootstrap();
  
        schedule(1);
    }

    /**
     * Schedule a DHT ping
     *
     * @param seconds poll in <code>secongs</codee> seconds
     */
    private void schedule(int seconds) {
        connectionManager.getScheduledExecutor().schedule(this, seconds, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        if (dSock != null) {
            processPeers();
            schedule(1);
        }
    }

    public void stopDHTReceivingThread() {
        receivingThreadRunning = false;
        try {
            receivingThread.join();
        } catch (Exception ex) {
            ex.printStackTrace(); // TODO
        }
        dSock.close();
    }

    private BigInteger searchDist(Contact c) {
        return unsigned(searchIDInt.xor(c.getId()));
    }

    public static BigInteger unsigned(BigInteger i) {
        if (i.signum() < 0) {
            return MASK.add(i);
        } else {
            return i;
        }
    }
    
    /**
     * Calculate a hash for the network, using the pubkicKey of the net
     *
     * @param maxLen length of the hash
     * @return the hash
     */
    private byte[] networkHash(int maxLen) {
        byte[] b = connectionManager.getAccessCfg().getPropertyBytes("network.signature", null);
        MessageDigest md = CryptoUtils.getMessageDigest();
        md.update("BitTorrent".getBytes());	// make sure the key differs from
        // other hashes created from the publicKey
        byte[] hash = md.digest(b);
        byte[] result = new byte[Math.min(maxLen, hash.length)];
        System.arraycopy(hash, 0, result, 0, result.length);
        return result;
    }

    private void ping(SocketAddress addr) throws IOException {
        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("ping"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("ping"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        m.put(new BencodeString("a"), a);
        
        sendPacket(addr, m);

        Logger.getLogger("").log(Level.INFO, "Using DHT bootstrap node: " + addr.toString());
    }

    private void getPeers(Contact c) throws IOException {
        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("get_peers"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("get_peers"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        a.put(new BencodeString("info_hash"), searchID);
        a.put(new BencodeString("want"), new BencodeString("n6"));
        m.put(new BencodeString("a"), a);

        sendPacket(c.getAddr(), m);
    }

    private synchronized void announcePeer(Contact c, BencodeString token) throws IOException {
        if (token == null) {
            return;
        }

        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("announce"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("announce_peer"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        a.put(new BencodeString("info_hash"), searchID);
        a.put(new BencodeString("implied_port"), new BencodeInt(0));
        a.put(new BencodeString("port"), new BencodeInt(dSock.getLocalPort()));
        a.put(new BencodeString("token"), token);
        m.put(new BencodeString("a"), a);

        sendPacket(c.getAddr(), m);
    }
    
    private void bootstrap() {
        try {
            for (String dhtNode : DHT_BOOTSTRAP_NODES) {
                String[] addressParts = dhtNode.split(":");
                ping(new InetSocketAddress(addressParts[0], Integer.parseInt(addressParts[1])));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void processPeers() {
        try {
            Vector<Contact> best = new Vector<Contact>();
            {
                Contact contact;
                while (best.size() < MAX_GOOD && null != (contact = peerQueue.poll())) {
                    if (!best.contains(contact) && getBad(contact) < MAX_BAD) {
                        best.add(contact);
                        //System.out.println("best: dist: " + searchDist(c).bitLength() + "  peer: " + c
                        //    + "  bad: " + getBad(c));
                    }
                }
            }
            for (Contact contact : best) {
                getPeers(contact);
                makeBad(contact);
                addQueue(contact, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendPacket(SocketAddress addr, BencodeObject o) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        o.write(out);
        byte[] buf = out.toByteArray();
        DatagramPacket p = new DatagramPacket(buf, buf.length, addr);
        dSock.send(p);
    }

    private void makeGood(Contact c) {
        peerBad.remove(c);
    }

    private int getBad(Contact c) {
        Integer i = peerBad.get(c);
        if (i == null) {
            return 0;
        }
        return i;
    }

    private void makeBad(Contact c) {
        peerBad.put(c, getBad(c) + 1);
    }

    private void addQueue(Contact c, boolean good) {
        if (!peerQueue.contains(c)) {
            peerQueue.add(c);
        }
        if (good) {
            makeGood(c);
        }
    }

    private void receivePacket(DatagramPacket packet) {
        try {
            BencodeObject object = Bencode.parseBencode(new ByteArrayInputStream(packet.getData(), 0, packet.getLength()));

            if (((BencodeMap) object).get(new BencodeString("y")).equals(new BencodeString("r"))) {
                String transactionID = ((BencodeMap) object).get(new BencodeString("t")).toString();
                BencodeMap response = (BencodeMap) ((BencodeMap) object).get(new BencodeString("r"));

                switch (transactionID) {
                    case "ping":
                        handlePing(packet, response);
                        break;
                    case "get_peers":
                        handlePeers(packet, response);
                        break;
                }
            } else if (((BencodeMap) object).get(new BencodeString("y")).equals(new BencodeString("e"))) {
                BencodeList e = (BencodeList) (((BencodeMap) object).get(new BencodeString("e")));
//                System.out.print("Error: ");
//                for (BencodeObject bo : e) {
//                    System.out.print(bo + " ");
//                }
//                System.out.println();
            }
        } catch (IOException ex) {
            ex.printStackTrace(); // TODO
        }
    }

    private void handlePing(DatagramPacket packet, BencodeMap response) throws IOException {
        BencodeString nodeId = (BencodeString) response.get(new BencodeString("id"));
        addQueue(new Contact(nodeId.getBytes(), packet.getSocketAddress()), true);
    }

    private void handlePeers(DatagramPacket packet, BencodeMap response) throws IOException {
        BencodeString nodeId = (BencodeString) response.get(new BencodeString("id"));
        Contact remotePeer = new Contact(nodeId.getBytes(), packet.getSocketAddress());
        addQueue(remotePeer, true);

        BencodeString nodes = (BencodeString) response.get(new BencodeString("nodes"));
        BencodeString nodes6 = (BencodeString) response.get(new BencodeString("nodes6"));
        BencodeList values = (BencodeList) response.get(new BencodeString("values"));

        if (nodes != null) {
            byte[] byteSequence = nodes.getBytes();
            for (int i = 0; i + NODE_ID_LEN + NODE_IPV4_LEN < byteSequence.length; i += NODE_ID_LEN + NODE_IPV4_LEN) {
                addQueue(new Contact(byteSequence, i, NODE_ID_LEN + NODE_IPV4_LEN), false);
            }
        }

        if (nodes6 != null) {
            byte[] byteSequence = nodes6.getBytes();
            for (int i = 0; i + NODE_ID_LEN + NODE_IPV6_LEN < byteSequence.length; i += NODE_ID_LEN + NODE_IPV6_LEN) {
                addQueue(new Contact(byteSequence, i, NODE_ID_LEN + NODE_IPV6_LEN), false);
            }
        }

        if (values != null) {
            for (BencodeObject val : values) {
                byte[] byteSequence = ((BencodeString) val).getBytes();
                InetSocketAddress addr = Contact.parseSocketAddress(byteSequence, 0, byteSequence.length);

                boolean ipAdded = false;
                for (Endpoint endPoint : connectionManager.getConnector().getIPs()) {
                    if (endPoint.getInetAddress().equals(addr.getAddress()) && endPoint.getPort() == addr.getPort()) {
                        ipAdded = true;
                        break;
                    }
                }

                if (!ipAdded) {
                    connectionManager.getConnector().addIP(addr.getAddress(),
                        addr.getPort(), null, "BitTorrent DHT", "", false);
                }
            }
        }

        long currentTimeMs = System.currentTimeMillis();
        BencodeString token = (BencodeString) response.get(new BencodeString("token"));
        if (!announcePeerLastTime.containsKey(remotePeer) || 
            announcePeerLastTime.get(remotePeer) + PEER_ANNOUNCE_INTERVAL_MS < currentTimeMs) {
            announcePeerLastTime.put(remotePeer, currentTimeMs);
            announcePeer(remotePeer, token);
        } else {
            FutureTask announcePeerTask = 
                new FutureTask(new AnnounceTask(
                    remotePeer, 
                    token,
                    announcePeerLastTime.get(remotePeer) + PEER_ANNOUNCE_INTERVAL_MS - currentTimeMs)
                );
            announcePeerTask.run();
        }
    }

    private class AnnounceTask implements Callable {
        final private Contact peer;
        final private BencodeString token;
        final private long remainingTime;
        
        AnnounceTask(Contact peer, BencodeString token, long remainingTime) {
            this.peer = peer;
            this.token = token;
            this.remainingTime = remainingTime;
        }
        
        @Override
        public String call() throws Exception {
            Thread.sleep(remainingTime);
            if (receivingThread.isAlive()) {
                announcePeer(peer, token);
            }
            return Thread.currentThread().getName();
        }
    }

    private void runReceivingThread() {
        try {
            byte[] buf = new byte[PACKET_LEN];
            DatagramPacket packet = new DatagramPacket(buf, PACKET_LEN);
            while (receivingThreadRunning) {
                dSock.receive(packet);
                receivePacket(packet);
            }
        } catch (IOException iOException) {
            iOException.printStackTrace(); // TODO
        }
    }
}
