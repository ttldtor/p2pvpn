/*
    Copyright 2008, 2009 Wolfgang Ginolas

    This file is part of P2PVPN.

    P2PVPN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    P2PVPN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with P2PVPN.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.dosse.upnp.UPnP;

public class UPnPPortForward {

    final private Vector<UPnPPortForwardListener> listeners;
    // calling any function from the UPnP library locks the thread 
    // and causes it to be initialized
    boolean initialized = false;

    ConnectionManager connectionManager;

    /**
     * Create a UPnP object
     *
     * @param connectionManager the ConnectionManager
     */
    public UPnPPortForward(ConnectionManager connectionManager) {
        listeners = new Vector<UPnPPortForwardListener>();

        this.connectionManager = connectionManager;
    }

    /**
     * Open a TCP port through UPnP
     */
    public void open() {
        try {
            int port = connectionManager.getServerPort();

            if (UPnP.isUPnPAvailable()) {
                if (!UPnP.isMappedTCP(port)) {
                    if (UPnP.openPortTCP(port)) {
                        Logger.getLogger("").log(Level.INFO, "UPnP port forwarding enabled.");
                    } else {
                        Logger.getLogger("").log(Level.WARNING, "UPnP port forwarding disabled.");
                    }
                } else {
                    Logger.getLogger("").log(Level.SEVERE, "UPnP port forwarding not enabled: port is already mapped!");
                }
            } else {
                Logger.getLogger("").log(Level.WARNING, "UPnP is not available!");
            }

            updateListeners();
        } catch (Throwable t) {
            Logger.getLogger("").log(Level.SEVERE, "Network error: ", t);
        }
    }

    /**
     * Close TCP port on router. If the port is not mapped, this function does
     * nothing.
     */
    public void close() {
        if (isAvailable() && isMapped()) {
            UPnP.closePortTCP(connectionManager.getServerPort());
        }

        updateListeners();
    }

    public boolean isAvailable() {
        initialized = true;
        return UPnP.isUPnPAvailable();
    }

    public boolean isMapped() {
        return UPnP.isMappedTCP(connectionManager.getServerPort());
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public String getExternalIP() {
        return UPnP.getExternalIP();
    }

    public boolean getInitialized() {
        return initialized;
    }

    private void updateListeners() {
        synchronized (listeners) {
            for (UPnPPortForwardListener l : listeners) {
                try {
                    l.upnpChanged(this);
                } catch (Throwable t) {
                    Logger.getLogger("").log(Level.WARNING, "", t);
                }
            }
        }
    }

    /**
     * Set the object of the upperlayer.
     *
     * @param l the upper layer
     */
    public void addListener(UPnPPortForwardListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public void removeListener(UPnPPortForwardListener l) {
        synchronized (listeners) {
            listeners.remove(l);
        }
    }
}
