#include <jni.h>
/* Header for class org_p2pvpn_tuntap_TunTapLinux */

#ifndef _Included_org_p2pvpn_tuntap_TunTapLinux
#define _Included_org_p2pvpn_tuntap_TunTapLinux
#ifdef __cplusplus
extern "C" {
#endif
	
#define JFUNC(type, name, ...) JNIEXPORT type JNICALL Java_org_p2pvpn_tuntap_TunTapLinux_##name (JNIEnv *env, jobject this, ##__VA_ARGS__)

/*
 * Class:     org_p2pvpn_tuntap_TunTapLinux
 * Method:    openTun
 * Signature: ()I
 */
JFUNC(jint, openTun);

/*
 * Class:     org_p2pvpn_tuntap_TunTapLinux
 * Method:    close
 * Signature: ()V
 */
JFUNC(void, close);

/*
 * Class:     org_p2pvpn_tuntap_TunTapLinux
 * Method:    write
 * Signature: ([BI)V
 */
JFUNC(void, write, jbyteArray, jint);

/*
 * Class:     org_p2pvpn_tuntap_TunTapLinux
 * Method:    read
 * Signature: ([B)I
 */
JFUNC(jint, read, jbyteArray);

#ifdef __cplusplus
}
#endif
#endif
