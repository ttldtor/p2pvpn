#include <jni.h>
/* Header for class org_p2pvpn_tuntap_TunTapWindows */

#ifndef _Included_org_p2pvpn_tuntap_TunTapWindows
#define _Included_org_p2pvpn_tuntap_TunTapWindows
#ifdef __cplusplus
extern "C" {
#endif

#define JFUNC(type, name, ...) JNIEXPORT type JNICALL Java_org_p2pvpn_tuntap_TunTapWindows_##name (JNIEnv *env, jobject this, ##__VA_ARGS__)

/*
 * Class:     org_p2pvpn_tuntap_TunTapWindows
 * Method:    openTun
 * Signature: ()I
 */
JFUNC(jint, openTun);

/*
 * Class:     org_p2pvpn_tuntap_TunTapWindows
 * Method:    close
 * Signature: ()V
 */
JFUNC(void, close);

/*
 * Class:     org_p2pvpn_tuntap_TunTapWindows
 * Method:    write
 * Signature: ([BI)V
 */
JFUNC(void, write, jbyteArray, jint);

/*
 * Class:     org_p2pvpn_tuntap_TunTapWindows
 * Method:    read
 * Signature: ([B)I
 */
JFUNC(jint, read, jbyteArray);

#ifdef __cplusplus
}
#endif
#endif
