/*
    Copyright 2017 Serge Zaitsev
    Copyright 2023 Nikolay Borodin <Monsterovich>

    This file is part of App indicator tray icon library for P2PVPN.

    P2PVPN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    P2PVPN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with P2PVPN.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "tray.h"

#define NOTIFICATION_TIMEOUT 5000

struct tray tray = {
    .icon = "",
    .tooltip = "",
    .menu = NULL,
    .size = 0
};

static void menu_cb(struct tray_menu *item) {
    jclass cls = (*item->env)->GetObjectClass(item->env, item->obj);
    jmethodID mid = (*item->env)->GetMethodID(item->env, cls, "clickCallback", "(I)V");
    if (mid == NULL) {
        return;
    }

    (*item->env)->CallVoidMethod(item->env, item->obj, mid, item->id);
}

JFUNC(void, addMenu, jstring text) {
    if (tray.menu == NULL) {
        tray.menu = (struct tray_menu*)malloc(sizeof(struct tray_menu));
        tray.size = 1;
    } else {
        tray.size++;
        tray.menu = (struct tray_menu*)realloc(tray.menu, tray.size * sizeof(struct tray_menu));
    }

    if (tray.menu == NULL) {
        printf("add menu realloc failed\n");
        return;
    }

    size_t id = tray.size - 1;
    memset(&(tray.menu[id]), 0, sizeof(struct tray_menu));
    const char *textChars = (*env)->GetStringUTFChars(env, text, NULL);

    tray.menu[id].env = env;
    tray.menu[id].obj = (*env)->NewGlobalRef(env, this);
    tray.menu[id].text = strdup(textChars);
    (*env)->ReleaseStringUTFChars(env, text, textChars);
    tray.menu[id].cb = menu_cb;
    tray.menu[id].id = id;
}

JFUNC(void, trayRun, jstring icon, jstring tooltip) {
    (void)this;

    const char *iconChars = (*env)->GetStringUTFChars(env, icon, NULL);
    tray.icon = iconChars;

    const char *tooltipChars = (*env)->GetStringUTFChars(env, tooltip, NULL);
    tray.tooltip = tooltipChars;

    if (tray_init(&tray) < 0) {
        printf("failed to create tray\n");
    } else {
        while (!tray_loop(1)) {}
    }

    for (size_t i = 0; i < tray.size; i++) {
        (*env)->DeleteGlobalRef(env, tray.menu[i].obj);
        free(tray.menu[i].text);
    }

    free(tray.menu);

    (*env)->ReleaseStringUTFChars(env, icon, iconChars);
    (*env)->ReleaseStringUTFChars(env, tooltip, tooltipChars);
}

JFUNC(void, showMessage, jstring caption, jstring text, jstring icon) {
    (void)this;

    const char *captionChars = (*env)->GetStringUTFChars(env, caption, NULL);
    const char *textChars = (*env)->GetStringUTFChars(env, text, NULL);
    const char *iconChars = (*env)->GetStringUTFChars(env, icon, NULL);

    NotifyNotification *notification = notify_notification_new(captionChars, textChars, iconChars);

    notify_notification_set_timeout(notification, NOTIFICATION_TIMEOUT);
    notify_notification_show(notification, NULL);

    g_object_unref(G_OBJECT(notification));

    (*env)->ReleaseStringUTFChars(env, caption, captionChars);
    (*env)->ReleaseStringUTFChars(env, text, textChars);
    (*env)->ReleaseStringUTFChars(env, icon, iconChars);
}

JFUNC(void, test) {
    (void)env;
    (void)this;
}
