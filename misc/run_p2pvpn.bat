@echo off
echo "Disabling P2P VPN interface"
netsh interface set interface "P2P VPN" disable
echo "Resetting interface"
netsh interface ipv4 reset "P2P VPN"
echo "Enabling P2P VPN interface"
netsh interface set interface "P2P VPN" enable
echo "Finally starting P2PVPN.exe"
P2PVPN.exe